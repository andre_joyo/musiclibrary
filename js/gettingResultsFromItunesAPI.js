var entity;
var keyword;
var checkedCountry=false;
var checkedExplicitContent=false;
var checkedLimit=false;
var counterClicksElementType=0;
var counterClicksCountry=0;
var counterClicksExplcitContent=0;
var countrySelected;
var explicitContentSelected;
var limitSelected;
var doSearch;
var result;
$(document).ready(function () {
        comproveLocalStorage();
        comproveKeyword();
        comproveCheckboxes();
        comproveOptionsSelected();                
})
function comproveLocalStorage() {
    if (localStorage.getItem('favorites') === null) {
        localStorage.setItem('favorites', '{"song":{"id":[]},"musicVideo":{"id":[]},"artist":{"id":[]},"album":{"id":[]}}');
    } else{
        favoritesObject=JSON.parse(localStorage.getItem("favorites"));    
        displayingFavorites();        
    }
}
function comproveKeyword() {
        $("#keyword").keyup(function () {
                setParameters();
        })
}
function comproveCheckboxes() {
        if ($('#chCountry').click(function () {
                if( $('#chCountry').attr('checked') ) {
                        checkedCountry=true;
                        doSearch=true;
                        $('#country').prop("disabled", false);               
                } else{
                        checkedCountry=false; 
                        $('#country').prop("disabled", true);               
                }
                if (doSearch==true) {
                        setParameters();
                }
        }))        
        if ($('#chExplicitContent').click(function () {
                if( $('#chExplicitContent').attr('checked') ) {
                        checkedExplicitContent=true;
                        doSearch=true;
                        $('#explicitContent').prop("disabled", false);                        
                } else{
                        checkedExplicitContent=false;
                        $('#explicitContent').prop("disabled", true);                        
                }
                if (doSearch==true) {
                        setParameters();
                }
        }))
        if ($('#chLimit').click(function () {
                if( $('#chLimit').attr('checked') ) {
                        checkedLimit=true;
                        doSearch=true;
                        $('#limit').prop("disabled", false);
                } else{
                        checkedLimit=false;
                        $('#limit').prop("disabled", true);               
                }
                if (doSearch==true) {
                        setParameters();
                }
        }));
        doSearch=false;
}
function comproveOptionsSelected() {
        $("#elementType").click(function () {
            counterClicksElementType++;
            if (counterClicksElementType%2==0) {
                entity=$("#elementType").val();
                setParameters();
            }
        })
        $("#country").click(function () {
            if (checkedCountry==true) {
                counterClicksCountry++;
                if (counterClicksCountry%2==0) {
                    if (doSearch==true) {
                        countrySelected=$("#country").val();
                        setParameters();
                    }
                }
            }        
        })
        $("#explicitContent").click(function () {
                if (checkedExplicitContent==true) {
                        counterClicksExplcitContent++;
                        if (counterClicksExplcitContent%2==0) {
                                if (doSearch==true) {
                                        explicitContentSelected=$("#explicitContent").val();
                                        setParameters();
                                }
                        }
                }
        })
        $("#limit").keyup(function () {
                if (checkedLimit==true) {
                        if (doSearch==true) {
                                limitSelected=$("#limit").val();
                                setParameters();
                        }
                }
        })
}
function setParameters() {
    keyword=$("#keyword").val();
    entity=$("#elementType").val();
    if (keyword.length>0) {
        load(keyword, entity, countrySelected, explicitContentSelected, limitSelected);
    } else{
        alert("Insert a keyword to search")
    }
}
function load(currentKeyword, currentEntity, currentCountry, currentExplicitContent, currentLimit){
    $.ajax({
        url:"http://itunes.apple.com/search?",
        data:{term:currentKeyword,entity:currentEntity, country: currentCountry, explicit: currentExplicitContent, limit: currentLimit},
        type:"GET",
        crossDomain: true,
        dataType: "jsonp",
        success: function(dataReturned, dataType){
            result=dataReturned;
            var results = dataReturned.results;
            instantiate();
        }
    })
}
function instantiate() {        
    $("#results").empty();
    result["results"].forEach(function(element, index) {
        if (element["wrapperType"]=="track") {
            if (element["kind"]=="song") {
                elementtrackName=element["trackName"];
                artistName=element["artistName"];
                trackTimeMillis=element["trackTimeMillis"];
                collectionName=element["collectionName"];
                trackViewUrl=element["trackViewUrl"];
                artworkUrl=element["artworkUrl100"];//console.log("artworkUrl: "+artworkUrl);
                trackPrice=element["trackPrice"];
                currency=element["currency"];
                trackExplicitness=element["trackExplicitness"];
                country=element["country"];
                genre=element["primaryGenreName"];
                previewUrl=element["previewUrl"];
                releaseDate=element["releaseDate"];
                trackId=element["trackId"];
                displayingSongs(elementtrackName, artistName, trackTimeMillis, collectionName, trackViewUrl, artworkUrl, trackPrice, trackExplicitness, country, genre, previewUrl, releaseDate, trackId, currency);
            } else {//kind=="music-video"
                trackName=element["trackName"];
                artistName=element["artistName"];
                trackTimeMillis=element["trackTimeMillis"];
                collectionName=element["collectionName"];
                artworkUrl=element["artworkUrl100"];
                genre=element["primaryGenreName"];
                trackPrice=element["trackPrice"];
                currency=element["currency"];
                releaseDate=element["releaseDate"];
                previewUrl=element["previewUrl"];
                trackViewUrl=element["trackViewUrl"];
                trackId=element["trackId"];
                displayingMusicVideos(trackName, artistName, trackTimeMillis, collectionName, artworkUrl, genre, trackPrice, releaseDate, previewUrl, trackViewUrl, trackId, currency);
            }
        } else if (element["wrapperType"]=="artist") {
            artistName=element["artistName"];
            primaryGenreName=element["primaryGenreName"];
            artistLinkUrl=element["artistLinkUrl"];
            artistId=element["artistId"];
            displayingArtists(artistName, primaryGenreName, artistLinkUrl, artistId);
        } else {//wrapperType=="collection"
            collectionName=element["collectionName"]; 
            trackCount=element["trackCount"]; 
            artistName=element["artistName"]; 
            releaseDate=element["releaseDate"]; 
            collectionPrice=element["collectionPrice"]; 
            currency=element["currency"];
            genre=element["primaryGenreName"]; 
            country=element["country"]; 
            explicitness=element["collectionExplicitness"]; 
            artworkUrl=element["artworkUrl100"]; 
            collectionId=element["collectionId"];
            displayingCollections(collectionName, trackCount, artistName, releaseDate, collectionPrice, genre, country, explicitness, artworkUrl, collectionId, currency);
        }
    });
    $("#results").append( "<div class='cleared'></div>");
    buttonsFavsToListen();
}
function getDuration(millis){
    const minutes = Math.floor(millis / 60000);
    const seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}
function displayingSongs(elementtrackName, artistName, trackTimeMillis, collectionName, trackViewUrl, artworkUrl, trackPrice, trackExplicitness, country, genre, previewUrl, releaseDate, trackId, currency) {
    duration=getDuration(trackTimeMillis);
    $("#results").append( "<div id='"+trackId+"' class='element song'><div class='contImg'><div class='img'><img src='"+artworkUrl+"'></div></div><ul><li>Track name: "+elementtrackName+"</li><li>Artist Name: "+artistName+"</li><li>Duration: "+duration+"</li><li>Collection Name: "+collectionName+"</li><li><a href='"+trackViewUrl+"'>link</a></li><li>Price: "+trackPrice+" "+currency+"</li><li>Explicit: "+trackExplicitness+"</li><li>Country: "+country+"</li><li>Genre: "+genre+"</li><li>Genre: "+releaseDate+"</li><li>ID: "+trackId+"</li></ul><button class='favorite'>Add favorite</button><audio controls><source src="+previewUrl+">Your browser does not support the audio element.</audio></div>");
}
function displayingMusicVideos(trackName, artistName, trackTimeMillis, collectionName, artworkUrl, genre, trackPrice, releaseDate, previewUrl, trackViewUrl, trackId, currency) {
    duration=getDuration(trackTimeMillis);    
    $("#results").append( "<div id='"+trackId+"' class='element musicVideo'><div class='contImg'><div class='img'><img src='"+artworkUrl+"'></div></div><ul><li>Track Name: "+trackName+"</li><li>Artist Name: "+artistName+"</li><li>Duration: "+duration+"</li><li>Collection Name: "+collectionName+"</li><li>Genre: "+genre+"</li><li>Price: "+trackPrice+" "+currency+"</li><li>Release Date: "+releaseDate+"</li><a href='"+trackViewUrl+"'>link</a><li>ID: "+trackId+"</li></ul><button class='favorite'>Add favorite</button><iframe class='frame' title='videoFrame' width='80%' src='"+previewUrl+"'></iframe></div>");
}
function displayingArtists(artistName, primaryGenreName, artistLinkUrl, artistId) {
    $("#results").append( "<div id='"+artistId+"' class='element artist'><div class='contImg'><div class='img'><img src='"+artworkUrl+"'></div></div><ul><li>Artist Name: "+artistName+"</li><li>Primary Genre: "+primaryGenreName+"</li><a href='"+artistLinkUrl+"'></a><li>Artist ID: "+artistId+"</li></ul><button class='favorite'>Add favorite</button></div>");
}
function displayingCollections(collectionName, trackCount, artistName, releaseDate, collectionPrice, genre, country, explicitness, artworkUrl, collectionId, currency) {
    $("#results").append( "<div id='"+collectionId+"' class='element album'><div class='contImg'><div class='img'><img src='"+artworkUrl+"'></div></div><ul><li>Collection Name: "+collectionName+"</li><li>Counter Tracks: "+trackCount+"</li><li>Artist Name: "+artistName+"</li><li>Release Date: "+releaseDate+"</li><li>Collection Price: "+collectionPrice+" "+currency+"</li><li>Genre: "+genre+"</li><li>Country: "+country+"</li><li>Explicit: "+explicitness+"</li><li>Collection ID: "+collectionId+"</li></ul><button class='favorite'>Add favorite</button></div>");
}

function buttonsFavsToListen() {
    $(".favorite").click(function () {
        id=$(this).parent().attr("id");
        elementType=$(this).parent().attr("class").slice(8);
        comproveFavoriteExistence(id, elementType);
    })
}
function comproveFavoriteExistence(id, elementType) {
    var found=false;
    favoritesObject=JSON.parse(localStorage.getItem("favorites"));    
    favoritesObject[elementType]["id"].forEach(function (element) {
        if (element==id) {
            found=true;
        }
    });
    if (found==false) {
        addFavorite(id, elementType);
    } else{
        console.log("ya está en favoritos");
    }
}
function addFavorite(id, elementType) {
    favoritesObject[elementType]["id"].push(id);    
    favoritesString=JSON.stringify(favoritesObject);
    localStorage.setItem("favorites", favoritesString);
    $("#favorites div ul").empty();
    displayingFavorites();
}
function removeFavorite(id, elementType) {
    $("#favorites div ul").empty();
    favoritesObject[elementType]["id"]=favoritesObject[elementType]["id"].filter(function(i) { return i !== id });
    favoritesString=JSON.stringify(favoritesObject);
    localStorage.setItem("favorites", favoritesString);    
    displayingFavorites();
}
function displayingFavorites() {
    for (var elementType in favoritesObject){
        $("#"+elementType+"Favorites ul").append(elementType);
        for (id in favoritesObject[elementType]["id"]){
            var currentID=favoritesObject[elementType]["id"][id];
            $("#"+elementType+"Favorites ul").append("<li class='"+elementType+"'>"+currentID+"<input type='button' id='"+currentID+"' class='buttonRemove' value='x'></li>");
            $("input[value*='x']").click(function(){
                removeFavorite($(this).attr('id'), $(this).parent().attr('class'));
            })
        }
    }
}