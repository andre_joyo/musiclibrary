$( document ).ready(function() {
    $.ajax({
        url: 'https://www.liferay.com/api/jsonws/country/get-countries/',
        // data: data,
        dataType: 'json',
        success: function( response ) {
            // console.log(response);
            response.forEach(element =>
                $('#country').append('<option value="' + element.a2 + '">' + element.nameCurrentValue + '</option>')
            )
        },
        error: function(err) {
            console.log('Loading countries failed');
        }
    });
});