# Music Library

Dynamic web page where do searchs about music using the iTunes Search API

## Getting Started

Deploying this application is intuitive

### Prerequisites

Basic knowledge of web development.

### Installing

Clone this repository on your machine and open index.html on your web browser.

## Author

* **André Joyo**  

## License

No license
